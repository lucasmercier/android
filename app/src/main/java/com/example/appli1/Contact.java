package com.example.appli1;

public class Contact {
    private String nom, prenom, tel;

    public Contact(String nom, String prenom, String tel) {
        this.nom = nom;
        this.prenom = prenom;
        this.tel = tel;
    }

    public String toString() {
        return (nom + " " + prenom + " " + tel);
    }
}

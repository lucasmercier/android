package com.example.appli1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.logging.Logger;

public class ContactAdd extends AppCompatActivity {

    private static ArrayList<Contact> contacts = new ArrayList<Contact>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_add);

        Intent ii = getIntent();
        Bundle b = ii.getExtras();

        Contact contact = new Contact(b.get("contact-nom").toString(), b.get("contact-prenom").toString(), b.get("contact-tel").toString());
        contacts.add(contact);

        ////////////////////////////////////////////////////
        writeToFile(contact.toString() + " \n", ContactAdd.this);
        String file = readFromFile(ContactAdd.this);

        TextView tv = (TextView) findViewById(R.id.tvFile);
        tv.setText(file);


        //////////////////////////////////////////////////////

        ArrayAdapter<Contact> adaptater = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, contacts);

        ListView listView = (ListView)findViewById(R.id.lv);
        listView.setAdapter(adaptater);

        Button boutonRetour = (Button)findViewById(R.id.boutonRetour);
        boutonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousWindow();
            }
        });
    }

    public void previousWindow () {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    private void writeToFile(String data,Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_APPEND));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("config.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
}

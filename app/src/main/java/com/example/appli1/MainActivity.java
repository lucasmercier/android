package com.example.appli1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int CODE_RES = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button boutonOk = (Button)findViewById(R.id.boutonOk);

        EditText nom = (EditText)findViewById(R.id.nom);
        String name = nom.getText().toString();

        EditText prenom = (EditText)findViewById(R.id.prenom);
        String surname = prenom.getText().toString();

        final EditText tel = (EditText)findViewById(R.id.tel);
        String phone = tel.getText().toString();

        boutonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText nom = (EditText)findViewById(R.id.nom);
                String name = nom.getText().toString();

                EditText prenom = (EditText)findViewById(R.id.prenom);
                String surname = prenom.getText().toString();

                final EditText tel = (EditText)findViewById(R.id.tel);
                String phone = tel.getText().toString();

                if(!name.equals("") && !surname.equals("") && !phone.equals("")) {
//                    Contact contact = new Contact(name, surname, phone);
                    Intent i = new Intent(MainActivity.this, ContactAdd.class);
                    i.putExtra("contact-nom", name);
                    i.putExtra("contact-prenom", surname);
                    i.putExtra("contact-tel", phone);
                    startActivity(i);
                }else{
                    Toast.makeText(MainActivity.this, "Remplis les champs wola ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
